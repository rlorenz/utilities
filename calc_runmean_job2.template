#!/bin/ksh
# File Name : calc_runmean_job.template
# Creation Date : 08/11/13
# Last Modified : 
# Created By : Ruth Lorenz (r.lorenz@unsw.edu.au), the fortran routines were 
#              created by Annette Hirsch (a.hirsch@unsw.edu.au)
# Purpose : calculate climatology out of GLACE write files, running mean over
#           30 years -> cascade of scripts
#
#     The fortran routine calc_avg.F90 needs several arguments passed to it
#     that can be set in here:
#     indir: path to files, contains ID of run from which files need to
#             be processed
#     outdir: output directory from where the read simulation can get the
#             postprocessed files (can be set in cable.nml,
#             cable_user%GLACE_DIR = '')
#     nodes: number of nodes used (usually 8x16 = 128)
#     basename : start of filename as set in cable_diag.F90 ('smcl_')
#     year1 : first year to start calculation of climatology
#     nyrs : number of year to calculate climatology over
#
# Usage: will be used as template for job cascade called by
#        calc_runmean_start.sh

#PBS -q normal
#PBS -N calcrunavg2
#PBS -j oe
#PBS -l ncpus=1,mem=17GB
#PBS -l walltime=14:00:00
#PBS -l wd
set -vx

##-----------------------
#function to determine if year is leapyear (1) or not (0)
##-----------------------

function leapyr {
  if [[ $(( ${testyr}%4 )) -eq 0  && $(( ${testyr}%100 )) -ne 0  || $(( ${testyr}%400 )) -eq 0 ]]; then
    typeset lpyr=1
  else
    typeset lpyr=0
  fi
  echo  "$lpyr"
}

#------------------------
# start main script
#------------------------
print YYYY #passed from last job

#find the next leap year
testyr=YYYY
print $testyr
while [[ $(leapyr) != 1 ]];do
    let testyr=$testyr+1
done
LLLL=$testyr
print $LLLL  

set -A YEAR YYYY $LLLL #[0] is the start year, [1] is the first leap year
set -A NYRS 30
RUNID='uamoa'

set -A DIR /short/$PROJECT/$USER/UM_ROUTDIR/$USER/$RUNID/smcl_ /short/$PROJECT/$USER/GLACE_output/clim_${YEAR[0]}_${NYRS}yrs/

set -A INDIR ${DIR[0]}${YEAR[0]}

set -A NODES 128
set -A FILENAME smcl_

print "The input directory has been set to ${DIR[0]}."
print "\n The average will be calculated starting ${YEAR[0]} over ${NYRS} years
 for ${NODES[0]} nodes. The output will be put into ${DIR[1]}.\n"

if [[ ! -d $INDIR ]]; then
    print "ERROR: No directory $INDIR, this run does not exist"
    exit
fi

if [[ ! -d ${DIR[1]} ]]; then
    mkdir ${DIR[1]}
fi

calc_avg_tools="/short/${PROJECT}/${USER}/utils/calc_avg_binFiles"
print "The executable directory is: ${calc_avg_tools}."

if [[ $calc_avg_tools == 'UNSET' ]]; then   
    print "\nYou have to set the path (calc_avg_tools) to point to your
            installation of the calc_avg_tools executables"
    exit
fi

cd $calc_avg_tools
	 
$calc_avg_tools/calc_avg_r8 ${DIR[0]} ${DIR[1]} ${NODES} ${FILENAME} ${YEAR[0]} ${YEAR[1]} ${NYRS}

let nxtyr=${YEAR[0]}+1
#set -A nxtyr $nxtyr
print $nxtyr

if [[ $nxtyr < 2031 ]]; then
    cat $calc_avg_tools/calc_runmean_job2.template | sed "s/\Y\Y\Y\Y/$nxtyr/" > job_file2.${nxtyr}

    qsub job_file2.${nxtyr}
fi

#============================ end of job =========================
