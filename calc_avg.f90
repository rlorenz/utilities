!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!

PROGRAM calc_avg_main

 IMPLICIT NONE

 ! base of filenames (created by CABLE) - passed to PROGRAM
 CHARACTER(LEN=300) :: filename, basename, indir, outdir
 CHARACTER(LEN=30) :: nodes, chyear1, chleapyr1, chyrs, chnode, chyear
 INTEGER :: nyrs, nnode, year1, leapyr1, y, n, year

 ! dimx = typically # landpoints over which the var is specified per timestep
 ! dimy = # timesteps, maximum nr as in first leapyear
 ! dimz = #nr of soil layers
 ! x = #landpts in actual year, should be == dimx
 ! t_stps = # timesteps in actual year, can be smaller than dimy
 ! z = #soil layers in actual year, should be == dimz
 INTEGER ::dimx, dimy, dimz, x, t_stps, z

 ! data arrays 
 REAL, DIMENSION(:,:,:,:), ALLOCATABLE :: indata
 REAL, DIMENSION(:,:,:), ALLOCATABLE :: climdata
 REAL, DIMENSION(:,:,:), ALLOCATABLE :: tmpdata

 IF( IARGC() > 0 ) THEN
    CALL GETARG(1, indir)    ! runid as defined from umuix
    CALL GETARG(2, outdir)   ! path to output directory
    CALL GETARG(3, nodes)    ! Number of nodes of control simulation
    CALL GETARG(4, basename) ! Basic file extension
    CALL GETARG(5, chyear1)    ! The first year of climatological period
    CALL GETARG(6, chleapyr1)    ! The first leap year of climatological period
    CALL GETARG(7, chyrs)     ! Number of years to compute climatology over
 ELSE
    STOP 'This program requires arguments to be passed to it.'
 ENDIF

 ! nnode, year1 and nyrs need to be integers -> convert chnode to nnode etc
 read(nodes,*) nnode
 read(chyear1,*) year1
 read(chleapyr1,*) leapyr1
 read(chyrs,*) nyrs


 ! Loop over the nodes
 print *, "Start loop over nodes"
 DO n=1,nnode

   write(chnode,'(I3.3)') n-1

   filename=TRIM(TRIM(basename)//TRIM(chnode))
   ! read info about the spec. binary data which was created by the
   ! host , i.e. how many points per timestep, how many timesteps, we use
   ! the first leap year for these dimensions
   !                   print *, "Read text file:", filename
   CALL read_txt_file( TRIM(TRIM(indir)//TRIM(chleapyr1)//'/'//TRIM(filename)), &
               dimx, dimy, dimz )
   ! if no landpoints on node -> exit and go to next
   IF ( dimx .eq. 0 ) THEN
     ! WRITE (*,*), filename//'.bin does not contain any data, go to next node'
     CYCLE
   ENDIF
   ! Use dimension for node n for leap years for allocation, fill with day one
   ! for non-leap years -> climatology long enough for all years
   !                  print *, "dimx: ", dimx, "dimy: ", dimy, "dimz: ", dimz, "#of years: ", nyrs  
   ALLOCATE( indata(dimz,dimx,dimy,nyrs) )
   ALLOCATE( climdata(dimz,dimx,dimy) )
   ALLOCATE( tmpdata(dimz,dimx,dimy) )

   ! read the binary data and store - assumes 1 file per year per node
   year=year1
   DO y=1,nyrs
     write(chyear,"(I4.4)") year
!     print *, "Year is ", year
     CALL read_txt_file( TRIM(TRIM(indir)//TRIM(chyear)//'/'//TRIM(filename)), &
               x, t_stps, z )
     IF (x .ne. dimx) THEN
        WRITE (*,*), 'ERROR: Land point dimension do not agree between files' 
        STOP 
     ENDIF
     IF (z .ne. dimz) THEN
       WRITE (*,*), 'ERROR: Soil moisture levels do not agree between files' 
       STOP 
     ENDIF

     CALL read_dat_file( TRIM(TRIM(indir)//TRIM(chyear)//'/'//TRIM(filename)), &
                             tmpdata, dimx, dimy, dimz, t_stps )
     indata(:,:,:,y)=tmpdata
     year = year+1
   ENDDO

   ! compute average across all years
   CALL comp_avg( indata, climdata, nyrs, dimx, dimy, dimz )

   ! Write to output binary file
   CALL write_dat_file( TRIM(TRIM(outdir)//filename), climdata, dimx,&
               dimy, dimz )

   DEALLOCATE( tmpdata )
   DEALLOCATE( climdata )
   DEALLOCATE( indata )

 ENDDO ! Loop over nodes

END PROGRAM calc_avg_main

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++!

SUBROUTINE read_txt_file( Lfilename, dimx, dimy, dimz )

 CHARACTER(LEN=*), INTENT(IN) :: Lfilename
 INTEGER, INTENT(OUT) :: dimx, dimy, dimz

 INTEGER, PARAMETER :: gok=0
 INTEGER :: gopenstatus
 CHARACTER(LEN=99) :: trash

 OPEN( UNIT=11,FILE=Lfilename//'.dat',STATUS="old",ACTION="read",&
       IOSTAT=gopenstatus )

 IF(gopenstatus==gok) THEN

   READ(11,*)
   READ (11,*), trash 
   READ (11,*), trash
   READ (11,*), trash
   READ (11,*), trash
   READ (11,*), trash

   READ (11,*), trash
   READ (11,*), dimx
   READ (11,*), trash
   READ (11,*), dimy
   READ (11,*), trash
   READ (11,*), dimz

 ELSE

   WRITE (*,*), Lfilename//'.dat',' NOT found to read'
   STOP

 ENDIF

 CLOSE(11)

END SUBROUTINE read_txt_file

!==========================================================================!

SUBROUTINE read_dat_file( Lfilename, ar_data, dimx, dimy, dimz, t_stps ) 

 INTEGER, INTENT(IN) :: dimx, dimy, dimz, t_stps
 REAL, INTENT(OUT), DIMENSION(dimz,dimx,dimy) :: ar_data
 CHARACTER(LEN=*), INTENT(IN) :: Lfilename

 INTEGER, PARAMETER :: gok=0
 INTEGER, SAVE :: rwd=0
 INTEGER :: gopenstatus
 INTEGER :: i,j,k

 OPEN(UNIT=22, FILE=Lfilename//'.bin', STATUS="OLD", &
       ACTION="READ", IOSTAT=gopenstatus, FORM="unformatted" )

 IF(gopenstatus==gok) THEN

   DO i=1,dimy
     DO j=1,dimx
       DO k=1,dimz
         IF (i .gt. t_stps .and. rwd==0) THEN
!           WRITE (*,*), 'No leap year, start over at beginning for last day of year'
           REWIND(22)
           rwd=rwd+1
         ENDIF
         READ(22), ar_data(k,j,i)
       ENDDO
     ENDDO
   ENDDO

 ELSE
   WRITE (*,*), Lfilename//'.bin',' NOT found for read'
   STOP
 ENDIF

 REWIND(22)
 rwd=0
 CLOSE(22)

END SUBROUTINE read_dat_file

!==========================================================================!

SUBROUTINE comp_avg( indata, avg_data, nyrs, dimx, dimy, dimz )

 INTEGER, INTENT(IN) :: dimx, dimy, dimz, nyrs
 REAL, INTENT(IN), DIMENSION(dimz, dimx, dimy, nyrs) :: indata
 REAL, INTENT(OUT), DIMENSION(dimz, dimx, dimy) :: avg_data
 REAL, DIMENSION(nyrs) :: sum_data
 INTEGER :: a,b,c

 DO a=1,dimy
   DO b=1,dimx
     DO c=1,dimz
       sum_data = indata(c,b,a,:)
       avg_data(c,b,a) = SUM(sum_data) / nyrs
     ENDDO
   ENDDO
 ENDDO

END SUBROUTINE comp_avg

!=======================================================================!

SUBROUTINE write_dat_file( Lfilename, ar_data, dimx, dimy, dimz )

 INTEGER, INTENT(IN) :: dimx, dimy, dimz
 REAL, INTENT(IN), DIMENSION(dimz,dimx,dimy) :: ar_data
 CHARACTER(LEN=*), INTENT(IN) :: Lfilename

 INTEGER, PARAMETER :: gok=0
 INTEGER :: gopenstatus
 INTEGER :: i,j,k

 OPEN(UNIT=33, FILE=Lfilename//'.bin', STATUS="unknown", &
       ACTION="write", IOSTAT=gopenstatus, FORM="unformatted" )

 IF(gopenstatus==gok) THEN

   DO i=1,dimy
     DO j=1,dimx
       DO k=1,dimz
         WRITE(33), ar_data(k,j,i)
       ENDDO
     ENDDO
   ENDDO

 ELSE
   WRITE (*,*), Lfilename//'.bin',' NOT found for write'
   STOP

 ENDIF

 CLOSE(33)

END SUBROUTINE write_dat_file
