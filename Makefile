#FC = gfortran
#FC = /apps/intel-fc/11.0.074/bin/intel64/ifort
FC = ifort
FFLAGS = -O3 -ip -xHost -g
#FFLAGS = -O0 -g -traceback  #slow options
#FFLAGS = -O2 -x f95-cpp-input

calc_avg_r4: calc_avg.f90
	$(FC) $(FFLAGS) -o calc_avg_r4 calc_avg.f90

calc_avg_r8: calc_avg.f90
	$(FC) $(FFLAGS) -i8 -r8 -o calc_avg_r8 calc_avg.f90
	#$(FC) $(FFLAGS) -check all -warn all -warn errors -i8 -r8 -o calc_avg_r8 calc_avg.f90

clean:
	rm -f *.o *~ *.mod calc_avg
