#!/bin/ksh
# File Name : calc_avg_start.sh
# Creation Date : 30/10/13
# Last Modified : 
# Created By : Ruth Lorenz (r.lorenz@unsw.edu.au), the fortran routines were 
#              created by Annette Hirsch (a.hirsch@unsw.edu.au)
# Purpose : calculate climatology out of GLACE write files
# Description: executing calc_avg_start will read output from GLACE write
#     per year and calculate the climatology over the indicated period.
#     This file is a wrapper for the fortran routine calc_avg.F90.
#     CABLE reads a namelist file (cable.nml) at runtime to configure
#     the model. Setting the variable cable_user%GLACE_STATUS to "WRITE" will
#     result in a CALL to subroutine cable_diag() being executed. This 
#     subroutine is passed (amongst other things) the data to be written to file
#     and the node #(N-1) from which it is written (starting from 000)
#     The data is written to a file(s) called smcl_000.bin [to smcl_(N-1).bin]. 
#     An additional file is output, smcl_000.dat (to smcl_(N-1).dat),
#     which contains information regarding smcl_???.bin necessary
#     to interpret/read this binary file in post-processing.
#     
#     In cable_user%GLACE_STATUS = 'WRITE' runs every timestep we write the soil
#     moisture (ssnow%wb) at every level (ms=6) to a file per node. Every file
#     contains timesteps for one run/resubmission (resubmission needs to be once
#     everyyear). After each year these files are moved from the rundirectory
#     into yearly folders. 
#     In GLACE_STATUS = 'READ' these kind of files can be read in again,
#     overwriting the calculated soil moisture at every level. It is crucial
#     to run write and read runs on the same number of processors to be able
#     to read files back in on a per node basis. In the case of postprocessing
#     in between, as here when calculating a climatology, we need to keep the
#     strucuture of one file per node. Every file can contain different numbers
#     of landpoints (dimx) depending on how the landpoints are distributed on
#     the nodes.
#     
#     Leap years: the read routine does not care if there are too many timesteps
#     in the files -> we can prepare the climatology with the size of leap years
#     in no leap years the last timesteps will not be used. Climatologies are
#     usually done using day-of-year instead of dates (ncl climatology routines,
#     fclimdex), for the 366th day of year which only exists in leap years data
#     from leap years are taken and day 1 of other years.
#
#     The fortran routine calc_avg.F90 needs several arguments passed to it
#     that can be set in here:
#     indir: path to files, contains ID of run from which files need to
#             be processed
#     outdir: output directory from where the read simulation can get the
#             postprocessed files (can be set in cable.nml,
#             cable_user%GLACE_DIR = '')
#     nodes: number of nodes used (usually 8x16 = 128)
#     basename : start of filename as set in cable_diag.F90 ('smcl_')
#     year1 : first year to start calculation of climatology
#     nyrs : number of year to calculate climatology over
#
# Usage: qsub calc_avg_start.sh

#PBS -q normal
#PBS -N calcavg
#PBS -j oe
#PBS -l ncpus=1,mem=17GB
#PBS -l walltime=14:00:00
#PBS -l wd

set -A YEAR 2071 2072 #[0] is the start year, [1] is the first leap year
set -A NYRS 30
RUNID='uamoa'

set -A DIR /short/$PROJECT/$USER/UM_ROUTDIR/$USER/$RUNID/smcl_ /short/$PROJECT/$USER/GLACE_output/clim_${YEAR[0]}_${NYRS}yrs/

set -A INDIR ${DIR[0]}${YEAR[0]}

set -A NODES 128
set -A FILENAME smcl_

print "The input directory has been set to ${DIR[0]}."
print "\n The average will be calculated starting ${YEAR[0]} over ${NYRS} years
 for ${NODES[0]} nodes. The output will be put into ${DIR[1]}.\n"

if [[ ! -d $INDIR ]]; then
    print "ERROR: No directory $INDIR, this run does not exist"
    exit
fi

if [[ ! -d ${DIR[1]} ]]; then
    mkdir ${DIR[1]}
fi

calc_avg_tools="/short/${PROJECT}/${USER}/utils/calc_avg_binFiles"
print "The executable directory is: ${calc_avg_tools}."

if [[ $calc_avg_tools == 'UNSET' ]]; then   
    print "\nYou have to set the path (calc_avg_tools) to point to your
            installation of the calc_avg_tools executables"
    exit
fi

cd $calc_avg_tools
#use "valgrind" option below for checking of memory leackages	 
$calc_avg_tools/calc_avg_r8 ${DIR[0]} ${DIR[1]} ${NODES} ${FILENAME} ${YEAR[0]} ${YEAR[1]} ${NYRS}
